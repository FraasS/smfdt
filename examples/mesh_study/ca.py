import sys
import numpy as np
import os
import matplotlib.pyplot as plt
import glob
import shutil
import warnings
sys.path.append('/mnt/fs2/home/sfraas/sandbox/smt00/scripts/')
from python import *

#
# functions
#

def GetArrayFromString(string):
  numberOfArrayElements = 1
  for i in string:
    if i == ",":
      numberOfArrayElements += 1

  counter = 0
  arrayFromString = [""] * numberOfArrayElements
  for i in string:
    if i == ",":
      counter += 1
    else:
      arrayFromString[counter] += i
  return arrayFromString


def GetLineTyp(list, string):
  # Define the line types
  listLineTypes = ["b", "g", "r", "c", "m", "y", "k"]
  for i in range(np.size(list)):
    if list[i] == string:
      return listLineTypes[i]

def GetVariableName(int):
  if int == 1:
    return "Mises"
  elif int == 2:
    return "dz"
  elif int == 3:
    return "dy"
  else:
    return "dx"

def WriteMaximumOrMinimum(path, name, valueList, append = False):
  if append:
    f = open(os.path.join(path + ".txt"), "a+")
    f.write("\n")
    f.write(name)
    f.write("\n# Line1: x, y, z, dx; # Line2: x , y, z, dy; # Line3: x , y, z, dz; # Line4: x , y, z, Mises")
    for i in range(np.size(valueList[:,0])):
      f.write("\n")
      f.write(os.path.join(str(valueList[i,0]) + ", " + str(valueList[i,1]) + ", " + str(valueList[i,2]) + ", " + str(valueList[i,3])))
      f.write("\n")
    f.close()
  else:
    f = open(os.path.join(path, name + ".csv"), "w")
    f.write("# Line1: x, y, z, dx\n# Line2: x , y, z, dy\n# Line3: x , y, z, dz\n# Line4: x , y, z, Mises")
    for i in range(len(valueList)):
      f.write("\n")
      f.write(os.path.join(str(valueList[i][1]) + ", " + str(valueList[i][2]) + ", " + str(valueList[i][3]) + ", " + str(valueList[i][4])))
    f.close()


#
# preProcessing
#

if sys.argv[len(sys.argv)-1] == "interpolateValues":
  patchNamesFoam = GetArrayFromString(sys.argv[3])
  patchNamesCalculix = GetArrayFromString(sys.argv[5])

  wssap = writeShearStressAndPressure(sys.argv[1], sys.argv[2], patchNamesFoam, int(sys.argv[4]))
  wssap.WritePressure()
  if (sys.argv[9] != "none"):
    wssap.WriteShearStress()

  foamCase = os.path.join("..",sys.argv[1])

  pathCalculixCase = os.path.join(sys.argv[7], "valueInterpolation.fbd")

  f = open(pathCalculixCase, "w")
  f.write("read ")
  f.write(foamCase)
  f.write(" foam")
  f.write("\nread ")
  f.write(sys.argv[6])
  f.write(" add")
  f.write("\nseto master\n")
  for i in range(np.size(patchNamesFoam)):
    f.write("seta master f ")
    f.write(patchNamesFoam[i])
    f.write("\n")
  f.write("comp master do\n")
  for i in range(np.size(patchNamesCalculix)):
    f.write("seta pressure n ")
    f.write(patchNamesCalculix[i])
    f.write("\n")
  f.write("comp pressure do\n")
  if (sys.argv[9] != "none"):
    for i in range(np.size(patchNamesCalculix)):
      f.write("seta shear n ")
      f.write(patchNamesCalculix[i])
      f.write("\n")
      f.write("comp shear do\n")
  f.write("map pressure master surf ds1\nsend pressure abq pres ds1 e1")
  if (sys.argv[9] != "none"):
    f.write("\nmap shear master surf ds2")
    f.write("\nsend shear abq pres ds2 e1")
    f.write("\nsend shear abq pres ds2 e2")
    f.write("\nsend shear abq pres ds2 e3")
  f.write("\nsend ")
  f.write(sys.argv[8])
  f.write(" abq")
  f.write("\n\nquit")
  f.close()

elif sys.argv[len(sys.argv)-1] == "writeSolverFile":
  #Get necessary arrays from string
  setName = GetArrayFromString(sys.argv[5])
  setNameForce = GetArrayFromString(sys.argv[7])
  rotationAxis = GetArrayFromString(sys.argv[9])
  
  #Get solver name consisting of two words
  solver = sys.argv[13].replace("_", " ")

  if sys.argv[6] == "False":
    force = False
  else:
    force = True

  #write the solver file
  wsf = writeSolverFile(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], setName, force, setNameForce, \
                        float(sys.argv[8]), rotationAxis, sys.argv[10], sys.argv[11], sys.argv[12], solver)

  wsf.WriteSolverFileGmsh()



elif sys.argv[len(sys.argv)-1] == "writeSolverInterpolationFile":
  wsf = writeSolverFile(sys.argv[1], sys.argv[2])
  wsf.WriteSolverInterpolateFile()

#
# postProcessing
#
  
elif sys.argv[len(sys.argv)-1] == "postProcessingWriteData":
  patchNameCalculix = GetArrayFromString(sys.argv[3])
  setName = GetArrayFromString(sys.argv[5])

  cP = calculixPost(calculixPostReadData(sys.argv[1], sys.argv[2], patchNameCalculix).Read())
  #write the values on pressure and suction side
  cP.WriteLineValuesToCsv("pressureSide", sys.argv[4], sys.argv[2], setName, [0,-1,0,1], ["x","y"], ["z>0"], ["y"])
  cP.WriteLineValuesToCsv("suctionSide", sys.argv[4], sys.argv[2], setName, [0,-1,0,1], ["x","y"], ["z<0"], ["y"])

  #write the maximum and minimum values
  maximum = cP.GetMaximum()
  minimum = cP.GetMinimum()
  WriteMaximumOrMinimum(sys.argv[1], "maximum", maximum)
  WriteMaximumOrMinimum(sys.argv[1], "minimum", minimum)



elif sys.argv[len(sys.argv)-1] == "postProcessing":
  folderList = (glob.glob('*case'))

  # plot figures
  valuesPressureSide = dict()
  valuesSuctionSide = dict()
  for i in folderList:
    try:
      valuesPressureSide[i] = np.genfromtxt(os.path.join(i, "pressureSide.csv"), delimiter = ",", comments = "#")
      valuesSuctionSide[i] = np.genfromtxt(os.path.join(i, "suctionSide.csv"), delimiter = ",", comments = "#")
    except IOError:
      warningMessage = \
        os.path.join("No values for pressure ore suctionSide found for case " + i + ": Figure will not be plotted")
      warnings.warn(warningMessage)
      pass

  # Create Folder for the figures
  try:
    shutil.rmtree("abbildungen")
  except FileNotFoundError:
    pass
  os.mkdir("abbildungen")

  #plot pressure and suction side
  for k in range(1,5):
    plt.figure(figsize=(10,4.8))
    for i in folderList:
      try:
        plt.title(os.path.join(GetVariableName(k)+ ", pressureSide"))
        plt.plot(-1*valuesPressureSide[i][:,3], valuesPressureSide[i][:,-k], GetLineTyp(folderList, i), label = i[:-5])
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
        plt.tight_layout()
      except KeyError:
        pass
    plt.savefig(os.path.join("abbildungen", GetVariableName(k)+"_pressureSide"))
    plt.close()

  for k in range(1,5):
    plt.figure(figsize=(10,4.8))
    for i in folderList:
      try:
        plt.title(os.path.join(GetVariableName(k)+ ", suctionSide"))
        plt.plot(-1*valuesSuctionSide[i][:,3], valuesSuctionSide[i][:,-k], GetLineTyp(folderList, i), label = i[:-5])
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
        plt.tight_layout()
      except KeyError:
        pass
    plt.savefig(os.path.join("abbildungen", GetVariableName(k)+"_suctionSide"))
    plt.close()

  #Check convergence

  #Define the convergence criteria
  convergenceCriteria = 5000
  f = open("konvergenz.txt", "w")
  f.write("Konvergenzkriterium = ")
  f.write(str(convergenceCriteria))
  f.close()

  valuesMaximum = dict()
  valuesMinimum = dict()
  for i in folderList:
    try:
      valuesMaximum[i] = np.genfromtxt(os.path.join(i, "maximum.csv"), delimiter = ",", comments = "#")
      valuesMinimum[i] = np.genfromtxt(os.path.join(i, "minimum.csv"), delimiter = ",", comments = "#")
    except IOError:
      warningMessage = \
        os.path.join("No values for Maximum ore Minimum found for case " + i + ": Deviation will not be calculated")
      warnings.warn(warningMessage)
      pass

  for i in range(np.size(folderList)):
    counter = 0
    for j in range(i + 1, np.size(folderList)):
      deviationMaximumList = []
      deviationMinimumList = []
      try:
        for k in range(np.size(valuesMaximum[folderList[i]][:,0])):
          deviationMaximum = \
            np.abs((valuesMaximum[folderList[i]][k,3] - valuesMaximum[folderList[j]][k,3])*100/valuesMaximum[folderList[i]][k,3])
          deviationMinimum = \
            np.abs((valuesMinimum[folderList[i]][k,3] - valuesMinimum[folderList[j]][k,3])*100/valuesMinimum[folderList[i]][k,3])
          if deviationMaximum <= convergenceCriteria and deviationMinimum <= convergenceCriteria:
            deviationMaximumList.append(os.path.join(GetVariableName(4-k) + ": " + str(deviationMaximum)))
            deviationMinimumList.append(os.path.join(GetVariableName(4-k) + ": " + str(deviationMinimum)))
      except KeyError:
        pass
      if np.size(deviationMinimumList) == 4 and np.size(deviationMinimumList) == 4:
        counter += 1
        f = open("konvergenz.txt", "a+")
        f.write("\n\n#########################################################################################")
        f.write("\n\nKonvergenzkriterium erreicht zwischen den Netzen ")
        f.write(folderList[i][:-5])
        f.write(" und ")
        f.write(folderList[j][:-5])
        f.write("\n\nWerte Maximum sind:\n")
        f.close()
        WriteMaximumOrMinimum("konvergenz", folderList[i][:-5], valuesMaximum[folderList[i]], True)
        WriteMaximumOrMinimum("konvergenz", folderList[j][:-5], valuesMaximum[folderList[j]], True)
        f = open("konvergenz.txt", "a+")
        f.write("\nDie Abweichung beträgt:\n\n")
        f.write(str(deviationMaximumList))
        f.write("\n\nWerte Minimum sind: \n")
        f.close()
        WriteMaximumOrMinimum("konvergenz", folderList[i][:-5], valuesMinimum[folderList[i]], True)
        WriteMaximumOrMinimum("konvergenz", folderList[j][:-5], valuesMinimum[folderList[j]], True)
        f = open("konvergenz.txt", "a+")
        f.write("\n\nDie Abweichung beträgt:\n")
        f.write(str(deviationMinimumList))
        f.write("\n\n#########################################################################################")
        f.close()
    if counter == 0 and i != np.size(folderList) - 1:
      f = open("konvergenz.txt", "a+")
      f.write("\n\nNetz ")
      f.write(folderList[i])
      f.write(": Konvergenzkriterium mit keinem anderen Netz erreicht")
      f.close()

  
else:
  warnings.warn("Don't now what to do, check flag in initialisation Allrun")

  

