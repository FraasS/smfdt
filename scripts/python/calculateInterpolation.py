import numpy
import os

class calculateInterpolation:
  def __init__(self, path, solverFile, elementName, setName = []):
    #
    # init variables
    #
    self.path_ = path
    self.solverFile_ = solverFile
    self.elementName_ = elementName
    self.setName_ = setName
    
    
  #
  # public
  #
  
  def CalculateNodeValues(self, Hex = False):
    if self.setName_[0] == "none":
      return 0
    
    #get the element number for correction of the interpolated values
    pathMshFile = os.path.join(self.path_, self.elementName_ + ".msh")
    elementNumberArray = numpy.genfromtxt(pathMshFile, usecols = 0, skip_header = 2, delimiter = ',')
    elementNumber = int(numpy.min(elementNumberArray))
    numberOfFaces = 0
    #load the data
    for i in range(numpy.size(self.setName_)):
      for k in range(1,3):
        pathDatFileX = os.path.join(self.path_, self.setName_[i] + "_ds" + str(k) + "e1.dlo")
        pathDatFileY = os.path.join(self.path_, self.setName_[i] + "_ds" + str(k) + "e2.dlo")
        pathDatFileZ = os.path.join(self.path_, self.setName_[i] + "_ds" + str(k) + "e3.dlo")
        convertfunc = lambda x: float(x.replace("P", ""))
        try:
          fy = numpy.genfromtxt(pathDatFileY, converters={1: convertfunc}, encoding=None, skip_header=1, delimiter=',')
          fx = numpy.genfromtxt(pathDatFileX, converters={1: convertfunc}, encoding=None, skip_header=1, delimiter=',')
          fz = numpy.genfromtxt(pathDatFileZ, converters={1: convertfunc}, encoding=None, skip_header=1, delimiter=',')

          #Correct the element numbering
          numberOfFaces = numpy.size(fx[:,0])
          for j in range(numberOfFaces):
            fx[j,0] -= elementNumber - 1
            fy[j,0] -= elementNumber - 1
            fz[j,0] -= elementNumber - 1
        except IOError:
          try:
            p = numpy.genfromtxt(pathDatFileX, converters={1: convertfunc}, encoding=None, skip_header=1, delimiter=',')
            numberOfFaces = numpy.size(p[:,0])
            #Correct the element numbering
            for j in range(numberOfFaces):
              p[j,0] -= elementNumber - 1
          except IOError:
            pass

    #Find the nodes for calculating the area and for interpolating the values
    arrayForInterpolating = self.__GetElementsWithCorrespondingNodesArray(Hex)
    
    firstOrder = bool(0) 
    if str(arrayForInterpolating[0,5]) == "inf":
      firstOrder = bool(1)
      
    for i in range(1,4):
      arrayForInterpolating[:,-i] = 0

    try:
      for i in range(numberOfFaces):
        arrayForInterpolating[i,-3] = fx[i,2]
        arrayForInterpolating[i,-2] = fy[i,2]
        arrayForInterpolating[i,-1] = fz[i,2]
    except NameError:
      pass
    
    nodesArray = self.__GetNodesOrElementsArray(Hex, returnNodesArray = True)
       
    #Get the normal Vector and the area of each element and calculate the pressure force
    nodesVector = self.__CalculateNodesVector(nodesArray, arrayForInterpolating, numberOfFaces, Hex, firstOrder)
    normalVector = self.__CalculateNormalVector(numberOfFaces, nodesVector, Hex, firstOrder)
    area = self.__CalculateArea(numberOfFaces, nodesVector, Hex, firstOrder)
    for i in range(numberOfFaces):
      arrayForInterpolating[i,9] = area[i]
      
    try:
      for i in range(numberOfFaces):
        arrayForInterpolating[i,-3] += p[i,2] * normalVector[i,0]
        arrayForInterpolating[i,-2] += p[i,2] * normalVector[i,1]
        arrayForInterpolating[i,-1] += p[i,2] * normalVector[i,2]
    except NameError:
      pass    
      
    #Build a list to find the index of the correspondingNodes
    numberOfFaceElements = numpy.size(arrayForInterpolating[:,0])
    if Hex:
      if firstOrder:
        lastNumber = 4
      else:
        lastNumber = 8
      nodeList = [0]*(lastNumber * numberOfFaceElements)
      for i in range(numberOfFaceElements):
        for k in range(lastNumber):
          nodeList[i + k * numberOfFaceElements] = int(arrayForInterpolating[i,k+1])
    else:
      nodeList = [0]*(3 * numberOfFaceElements)
      if firstOrder:
        startNumber = 1
      else:
        startNumber = 4
      for i in range(numberOfFaceElements):
        for k in range(3):
          nodeList[i + k * numberOfFaceElements] = int(arrayForInterpolating[i,k+startNumber])
          
    minInterpolationNode = int(numpy.min(nodeList))
    maxInterpolationNode = int(numpy.max(nodeList))
    
    listForIteration = []
    nodeListCopy = nodeList[:]
    nodeListCopy.sort()
    for i in range(minInterpolationNode, maxInterpolationNode + 1):
      if nodeListCopy[0] == i:
        listForIteration.append(i)
        for j in range(numpy.size(nodeListCopy)):
          if nodeListCopy[0] == i:
            nodeListCopy.pop(0)
          else:
            break

    correspondingNodes = dict()
    counter = 0
    for i in listForIteration:
      counter += 1
      indexList = []
      indexList.append(i)
      nodeListCopy = nodeList[:]
      for j in range(nodeList.count(i)):
        try:
          index = nodeListCopy.index(i) + j
          nodeListCopy.remove(i)
          for k in range(1,8):
            if index >= k * numberOfFaceElements and index < (k+1) * numberOfFaceElements:
              index -= k * numberOfFaceElements
          indexList.append(index)    
        except TypeError:
          pass
      correspondingNodes[counter] = indexList
      
    factor = 1.0/3
    if Hex:
      factor = 1.0/4
      
    Fx = numpy.zeros((len(correspondingNodes) ,3))
    Fy = numpy.zeros((len(correspondingNodes) ,3))
    Fz = numpy.zeros((len(correspondingNodes) ,3))
    for key in correspondingNodes:
      Fx[key-1,0] = correspondingNodes[key][0]
      Fy[key-1,0] = correspondingNodes[key][0]
      Fz[key-1,0] = correspondingNodes[key][0]
      Fx[key-1,1] = 1
      Fy[key-1,1] = 2
      Fz[key-1,1] = 3
      if Hex and firstOrder == False:
        for i in range(numpy.size(arrayForInterpolating[correspondingNodes[key][1]])):
          if arrayForInterpolating[correspondingNodes[key][1], i] == correspondingNodes[key][0]:
            nodeIndex = i
        if nodeIndex <= 4:
          factor = -1.0/12
        else:
          factor = 1.0/3
      for i in range(nodeList.count(correspondingNodes[key][0])):
        Fx[key-1,2] += factor*(arrayForInterpolating[correspondingNodes[key][i+1],10]*arrayForInterpolating[correspondingNodes[key][i+1],9])
        Fy[key-1,2] += factor*(arrayForInterpolating[correspondingNodes[key][i+1],11]*arrayForInterpolating[correspondingNodes[key][i+1],9])
        Fz[key-1,2] += factor*(arrayForInterpolating[correspondingNodes[key][i+1],12]*arrayForInterpolating[correspondingNodes[key][i+1],9])
    force = numpy.zeros((3 * len(correspondingNodes) ,3))
    for i in range(numpy.size(Fx[:,0])):
      force[i] = Fx[i]
      force[i + numpy.size(Fx[:,0])] = Fy[i]
      force[i + 2 * numpy.size(Fx[:,0])] = Fz[i]
    return force
  
  
  def CalculateLineValues(self, coordinateIndex, splitLine, normalVector, lineDirection, dataArray, Hex = False):
    elementsWithCorrespondingNodesArray = self.__GetElementsWithCorrespondingNodesArray(Hex)
    nodesArray = self.__GetNodesOrElementsArray(Hex, returnNodesArray = True)
    #Switch the coordinate-axis for finding the needed elements if slope is smaller than 0.5
    index = self.__GetIndex(coordinateIndex, splitLine)

    #Build a dict with the information about the elements needed
    counter = 0
    elementsDict = dict()
    for i in range(numpy.size(elementsWithCorrespondingNodesArray[:,0])):
      counterNegativ = 0
      counterPositiv = 0
      for k in range(1,9):
        try:
          #Get the corresponding value on the line
          lineValue = self.__GetCoordinateFromLine(splitLine, nodesArray[int(elementsWithCorrespondingNodesArray[i, k] - 1), index[1]])
          if nodesArray[int(elementsWithCorrespondingNodesArray[i,k] - 1), index[0]] - lineValue > 0:
            counterPositiv += 1
          elif nodesArray[int(elementsWithCorrespondingNodesArray[i,k] - 1), index[0]] - lineValue < 0:
            counterNegativ += 1
          else:
            elementsDict[counter] = i
        except OverflowError:
          pass
    
      if counterPositiv > 0 and counterNegativ > 0:
        elementsDict[counter] = i
      counter += 1
    firstOrder = bool(0)
    if str(elementsWithCorrespondingNodesArray[0, 5]) == "inf":
      firstOrder = bool(1)
    
    #Get the elements with the right orientation of the normal vector
    numberOfFaces = numpy.size(elementsWithCorrespondingNodesArray[:,0])
    nodesVector = self.__CalculateNodesVector(nodesArray, elementsWithCorrespondingNodesArray, numberOfFaces, Hex, firstOrder)
    if normalVector[0] != "none":
      normalVector[0] = normalVector[0].replace(" ","")
      normalVectorArray = self.__CalculateNormalVector(numberOfFaces, nodesVector, Hex, firstOrder)
      elementsDictCopy = elementsDict.copy()
      if normalVector[0][1] == ">":
        for key in elementsDictCopy:
          if normalVectorArray[key,self.__GetIndexFromString(normalVector)] < 0:
            del elementsDict[key]
      else:
        for key in elementsDictCopy:
          if normalVectorArray[key,self.__GetIndexFromString(normalVector)] > 0:
            del elementsDict[key]

    #Get a list with the intersection points and the corresponding nodes number
    intersectionPoints = self.__CalculateIntersectionPoint(elementsWithCorrespondingNodesArray, nodesArray, \
                         splitLine, coordinateIndex, elementsDict, nodesVector, Hex, firstOrder)

    #Get the index of the corresponding nodes in the dataArray for calculating the interpolated values
    nodesNumberInDataArray = [0]*(numpy.size(dataArray[:,0]))
    for i in range(numpy.size(dataArray[:,0])):
      nodesNumberInDataArray[i] = dataArray[i,0]
    indexDict = dict()
    for i in range(numpy.size(intersectionPoints)):
      index = [0]*(2)
      try:
        for k in range(2):
          index[k] = nodesNumberInDataArray.index(intersectionPoints[i][k])
        indexDict[i] = index
      except IndexError:
        break
      except ValueError:
        #Only values on a specific surface are wanted
        pass

    calculatedLineValuesArray = numpy.zeros((len(indexDict),9))
    counter = 0
    for key in indexDict:
      #Calculate the weighting factor
      vectorAB = numpy.zeros(3)
      vectorAIntersectionPoint = numpy.zeros(3)
      for k in range(3):
        vectorAB[k] = dataArray[indexDict[key][0]][k+1] - dataArray[indexDict[key][1]][k+1]
        vectorAIntersectionPoint[k] = dataArray[indexDict[key][0]][k+1] - intersectionPoints[key][k+2]

      lengthVectorAB = numpy.linalg.norm([vectorAB[0] + vectorAB[1] + vectorAB[2]])
      lengthVectorAIntersectionPoint = numpy.linalg.norm([vectorAIntersectionPoint[0] + vectorAIntersectionPoint[1] \
                                                          + vectorAIntersectionPoint[2]])
      weights = 1 - numpy.abs(lengthVectorAIntersectionPoint/lengthVectorAB)

      #Calculate the interpolated values with the given weights
      for k in range(4,8):
        calculatedLineValuesArray[counter,k+1] = weights * dataArray[indexDict[key][0]][k] + \
                                             (1-weights) * dataArray[indexDict[key][1]][k]

      for k in range(0,5):
        calculatedLineValuesArray[counter,k] = intersectionPoints[key][k]

      counter += 1

    if lineDirection[0] != "none":
      lineDirectionIndex = self.__GetIndexFromString(lineDirection) + 2
      lineDirectionRowSorted = numpy.sort(calculatedLineValuesArray, axis = 0)[:,lineDirectionIndex]
      calculatedLineValuesArrayCopy = numpy.copy(calculatedLineValuesArray)
      for i in range(numpy.size(lineDirectionRowSorted)):
        for k in range(numpy.size(lineDirectionRowSorted)):
          if calculatedLineValuesArrayCopy[k, lineDirectionIndex] == lineDirectionRowSorted[i]:
            for j in range(9):
              calculatedLineValuesArray[i,j] = calculatedLineValuesArrayCopy[k,j]

    return calculatedLineValuesArray
  
  
  def CalculateAverageValues(self, dataArray, Hex = False):
    #Find the nodes for calculating the area and the averaged values
    elementsWithCorrespondingNodesArray = self.__GetElementsWithCorrespondingNodesArray(Hex)
    
    firstOrder = bool(0) 
    if str(elementsWithCorrespondingNodesArray[0,5]) == "inf":
      firstOrder = bool(1)
    
    #Get the the area of each element 
    numberOfFaces = numpy.size(elementsWithCorrespondingNodesArray[:,0])
    nodesArray = self.__GetNodesOrElementsArray(Hex, returnNodesArray = True)
    nodesVector = self.__CalculateNodesVector(nodesArray, elementsWithCorrespondingNodesArray, numberOfFaces, Hex, firstOrder)
    area = self.__CalculateArea(numberOfFaces, nodesVector, Hex, firstOrder)
    
    #Get number of nodes per face
    nodesPerFace = 0
    for i in range(numpy.size(elementsWithCorrespondingNodesArray[0,:]) - 1):
      if str(elementsWithCorrespondingNodesArray[0,i +1]) != "inf":
        nodesPerFace += 1
    
    #Get the index of the corresponding nodes in the dataArray for calculating the averaged values
    nodesNumberInDataArray = [0]*(numpy.size(dataArray[:,0]))
    for i in range(numpy.size(dataArray[:,0])):
      nodesNumberInDataArray[i] = dataArray[i,0]
    indexDict = dict()
    for i in range(numpy.size(elementsWithCorrespondingNodesArray[:,0])):
      index = [0]*(nodesPerFace)
      try:
        for k in range(numpy.size(index)):
          index[k] = nodesNumberInDataArray.index(elementsWithCorrespondingNodesArray[i][k+1])
        indexDict[i] = index
      except IndexError:
        break
      except ValueError:
        #Only values on a specific surface are wanted
        pass
    
    #Calculate the area averaged values
    averageValues = numpy.zeros(4)
    totalArea = 0
    for key in indexDict:
      totalArea += area[key]
      for i in range(numpy.size(averageValues)):
        faceAverage = 0
        for k in range(nodesPerFace):
          # Use the arithmetic average for the faceAverage. 
          #This is not completely correct, but the error that results is negligible.
          faceAverage += (dataArray[indexDict[key][k]][-4+i]) / nodesPerFace
        averageValues[i] += (faceAverage * area[key]) 
    averageValues /= totalArea
    
    return averageValues
         
  #
  # private functions
  #
     
  def __GetElementsWithCorrespondingNodesArray(self, Hex = False): 
    #get the element number for correction of the interpolated values
    pathMshFile = os.path.join(self.path_, self.elementName_ + ".msh")
    elementNumberArray = numpy.genfromtxt(pathMshFile, usecols = 0, skip_header = 2, delimiter = ',')
    elementNumber = int(numpy.min(elementNumberArray))
    numberOfFaces = 0

    #load the data
    try:
      pathDatFileX = os.path.join(self.path_, self.setName_[0] + "_ds" + str(1) + "e1.dlo")

      convertfunc = lambda x: float(x.replace("P", ""))
      p = numpy.genfromtxt(pathDatFileX, converters={1: convertfunc}, encoding=None, skip_header=1, delimiter=',')

    except IOError:
      pathDatFileX = os.path.join(self.path_, self.setName_[0] + "_ds" + str(2) + "e1.dlo")

      convertfunc = lambda x: float(x.replace("P", ""))
      p = numpy.genfromtxt(pathDatFileX, converters={1: convertfunc}, encoding=None, skip_header=1, delimiter=',')
    
    numberOfFaces = numpy.size(p[:,0])
    #Correct the element numbering
    for j in range(numberOfFaces):
       p[j,0] -= elementNumber - 1

    elementsArray = self.__GetNodesOrElementsArray(Hex)
    
    #Find the nodes for calculating the area and for interpolating the values
    arrayForInterpolating = numpy.full((numberOfFaces, 13), numpy.inf)

    arrayForInterpolating = self.__GetCorrespondingNodes(p, elementsArray, arrayForInterpolating, Hex) 
  
    return arrayForInterpolating
  
  
  def __GetNodesOrElementsArray(self, Hex = False, returnNodesArray = False):
    if Hex:
      #Create a temporary file and load the nodes and elements
      pathSolverFile = os.path.join(self.path_, self.solverFile_ + ".inp")
      pathTempSolverFile = os.path.join(self.path_, self.solverFile_ + "_temp.inp")
      
      startLineElements = 0
      l = list(open(pathSolverFile, "r"))
      f = open(pathTempSolverFile, "w")
      for no, line in enumerate(l):
        if "ELEMENT" and "C3D20" in line:
          startLineElements = no
        if "*NSET" in line:
          breakingLine = no
          break
      if startLineElements == 0:
        startLineElements = breakingLine
      for i in range(startLineElements):
        f.write(l[i])
      for i in range(int((breakingLine - startLineElements)/2)):
        i = i * 2 + startLineElements + 1
        f.write(str(l[i:i+1])[2:-4] + str(l[i+1:i+2])[2:-4])
        f.write("\n")
      f.close()

      #Find the number of Elements and Nodes
      nodesElementsNumber = numpy.genfromtxt(pathTempSolverFile, comments = '*', delimiter = ',', usecols = 0, skip_header = 3)
      numberOfNodes = int(numpy.max(nodesElementsNumber))
      nodesArray = numpy.genfromtxt(pathTempSolverFile, comments = '*', delimiter = ',', skip_header = 4, max_rows=numberOfNodes)
      elementsArrayTemp = numpy.genfromtxt(pathTempSolverFile, comments = '*', delimiter = ',', \
                                       skip_header = 5 + numberOfNodes)
      
      
      # Switch to an ascending order
      sortArray = numpy.argsort(elementsArrayTemp,0)
      elementsArray = numpy.zeros((numpy.size(elementsArrayTemp[:,0]),numpy.size(elementsArrayTemp[0,:])))
      for i in range(numpy.size(elementsArray[:,0])):
        elementsArray[i] = elementsArrayTemp[sortArray[i,0]]
                              
      os.remove(pathTempSolverFile)
    
    else:
      #Load the nodes
      pathInterpolateFile = os.path.join(self.path_, self.solverFile_ + "_interpolate.inp")
      nodesArray = numpy.genfromtxt(pathInterpolateFile, comments = '*', delimiter = ',', usecols = 0, skip_header = 3)
      numberOfNodes = self.__GetNumber(nodesArray)
      nodesArray = numpy.genfromtxt(pathInterpolateFile, comments = '*', delimiter = ',', skip_header = 3, max_rows=numberOfNodes)
      #Load the elements
      elementsArray = numpy.genfromtxt(pathInterpolateFile, comments = '*', delimiter = ',', usecols = 0, skip_header = 3 + numberOfNodes)
      elementsMin = elementsArray[0]
      elementsMax = self.__GetNumber(elementsArray)
      elementsArray = numpy.genfromtxt(pathInterpolateFile, comments = '*', delimiter = ',', skip_header = 3 + numberOfNodes, \
                                       max_rows = elementsMax - elementsMin + 1)
      #Correct the elements numbering
      for i in range(numpy.size(elementsArray[:,0])):
        elementsArray[i,0] = i + 1
    
    if returnNodesArray:
      return nodesArray
    
    else:
      return elementsArray


  def __GetCorrespondingNodes(self, data, elementsArray, arrayForInterpolating, Hex = False):
    if Hex:
    #Hexaeder second order
      for i in range(numpy.size(data[:,0])):
        try:
          if int(data[i,1]) == 1:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),1]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),2]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),3]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),4]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),12]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),9]
            arrayForInterpolating[i,7] = elementsArray[int(data[i,0]-1),10]
            arrayForInterpolating[i,8] = elementsArray[int(data[i,0]-1),11]
          elif int(data[i,1]) == 2:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),5]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),8]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),7]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),6]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),13]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),16]
            arrayForInterpolating[i,7] = elementsArray[int(data[i,0]-1),15]
            arrayForInterpolating[i,8] = elementsArray[int(data[i,0]-1),14]
          elif int(data[i,1]) == 3:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),1]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),5]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),6]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),2]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),9]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),17]
            arrayForInterpolating[i,7] = elementsArray[int(data[i,0]-1),13]
            arrayForInterpolating[i,8] = elementsArray[int(data[i,0]-1),18]
          elif int(data[i,1]) == 4:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),2]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),6]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),7]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),3]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),10]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),18]
            arrayForInterpolating[i,7] = elementsArray[int(data[i,0]-1),14]
            arrayForInterpolating[i,8] = elementsArray[int(data[i,0]-1),19]
          elif int(data[i,1]) == 5:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),3]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),7]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),8]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),4]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),11]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),19]
            arrayForInterpolating[i,7] = elementsArray[int(data[i,0]-1),15]
            arrayForInterpolating[i,8] = elementsArray[int(data[i,0]-1),20]
          else:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),4]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),8]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),5]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),1]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),12]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),20]
            arrayForInterpolating[i,7] = elementsArray[int(data[i,0]-1),16]
            arrayForInterpolating[i,8] = elementsArray[int(data[i,0]-1),17]
        except IndexError:
          #Hexaeder first order
          pass
      
    else:
      for i in range(numpy.size(data[:,0])):
        try:
          #Tetraeder second order  
          if int(data[i,1]) == 1:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),1]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),2]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),3]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),7]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),5]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),6]
          elif int(data[i,1]) == 2:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),1]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),4]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),2]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),5]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),8]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),9]
          elif int(data[i,1]) == 3:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),2]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),4]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),3]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),6]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),9]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),10]
          else:
            arrayForInterpolating[i,0] = elementsArray[int(data[i,0]-1),0]
            arrayForInterpolating[i,1] = elementsArray[int(data[i,0]-1),3]
            arrayForInterpolating[i,2] = elementsArray[int(data[i,0]-1),4]
            arrayForInterpolating[i,3] = elementsArray[int(data[i,0]-1),1]
            arrayForInterpolating[i,4] = elementsArray[int(data[i,0]-1),7]
            arrayForInterpolating[i,5] = elementsArray[int(data[i,0]-1),10]
            arrayForInterpolating[i,6] = elementsArray[int(data[i,0]-1),8]
        except IndexError:
            #Tetraeder first order
            pass

    return arrayForInterpolating
  
  
  def __CalculateNodesVector(self, nodesArray, arrayForInterpolating, numberOfFaces, Hex = False, firstOrder = False):
    nodesVector = {"nodesVectorA" : 0, "nodesVectorB" : 0, "nodesVectorC" : 0, "nodesVectorD" : 0, "nodesVectorE" : 0, "nodesVectorF" : 0, "nodesVectorG" : 0, \
                   "nodesVectorH" : 0,"nodesVectorBC" : 0, "nodesVectorAE" : 0, "nodesVectorAD" : 0, "nodesVectorDB" : 0, "nodesVectorDE" : 0, "nodesVectorDF" : 0, \
                   "nodesVectorDC" : 0, "nodesVectorEB" : 0, "nodesVectorEF" : 0, "nodesVectorAB" : 0, "nodesVectorAC" : 0, "nodesVectorCF" : 0, "nodesVectorAF" : 0, \
                   "nodesVectorFB" : 0, "nodesVectorFG" : 0, "nodesVectorEG" : 0, "nodesVectorEH" : 0, "nodesVectorED" : 0, "nodesVectorHG" : 0, "nodesVectorHC" : 0, \
                   "nodesVectorBG" : 0, "nodesVectorGC" : 0, "nodesVectorDH" : 0,}
    
    for key in nodesVector:
      nodesVector[key] = numpy.zeros((numberOfFaces, 3))
      
    for i in range(numberOfFaces):
      for k in range(3):
        try:
          nodesVector["nodesVectorA"][i,k] = nodesArray[int(arrayForInterpolating[i,1] - 1), k + 1]
          nodesVector["nodesVectorB"][i,k] = nodesArray[int(arrayForInterpolating[i,2] - 1), k + 1]
          nodesVector["nodesVectorC"][i,k] = nodesArray[int(arrayForInterpolating[i,3] - 1), k + 1]
          nodesVector["nodesVectorD"][i,k] = nodesArray[int(arrayForInterpolating[i,4] - 1), k + 1]
          nodesVector["nodesVectorE"][i,k] = nodesArray[int(arrayForInterpolating[i,5] - 1), k + 1]
          nodesVector["nodesVectorF"][i,k] = nodesArray[int(arrayForInterpolating[i,6] - 1), k + 1]
          nodesVector["nodesVectorG"][i,k] = nodesArray[int(arrayForInterpolating[i,7] - 1), k + 1]
          nodesVector["nodesVectorH"][i,k] = nodesArray[int(arrayForInterpolating[i,8] - 1), k + 1]
        except OverflowError:
          pass
        
      nodesVector["nodesVectorAB"][i] = nodesVector["nodesVectorB"][i] - nodesVector["nodesVectorA"][i]
      nodesVector["nodesVectorAD"][i] = nodesVector["nodesVectorD"][i] - nodesVector["nodesVectorA"][i]
      nodesVector["nodesVectorAE"][i] = nodesVector["nodesVectorE"][i] - nodesVector["nodesVectorA"][i]
      nodesVector["nodesVectorEF"][i] = nodesVector["nodesVectorF"][i] - nodesVector["nodesVectorE"][i]
      nodesVector["nodesVectorFB"][i] = nodesVector["nodesVectorB"][i] - nodesVector["nodesVectorF"][i]
      nodesVector["nodesVectorBC"][i] = nodesVector["nodesVectorC"][i] - nodesVector["nodesVectorB"][i]
      nodesVector["nodesVectorDC"][i] = nodesVector["nodesVectorC"][i] - nodesVector["nodesVectorD"][i]
    
    if Hex:
      if firstOrder:
        for i in range(numberOfFaces):
          nodesVector["nodesVectorDB"][i] = nodesVector["nodesVectorB"][i] - nodesVector["nodesVectorD"][i]
      else:
        for i in range(numberOfFaces):
          nodesVector["nodesVectorAF"][i] = nodesVector["nodesVectorF"][i] - nodesVector["nodesVectorA"][i]
          nodesVector["nodesVectorFG"][i] = nodesVector["nodesVectorG"][i] - nodesVector["nodesVectorF"][i]
          nodesVector["nodesVectorEG"][i] = nodesVector["nodesVectorG"][i] - nodesVector["nodesVectorE"][i]
          nodesVector["nodesVectorEH"][i] = nodesVector["nodesVectorH"][i] - nodesVector["nodesVectorE"][i]
          nodesVector["nodesVectorED"][i] = nodesVector["nodesVectorD"][i] - nodesVector["nodesVectorE"][i]
          nodesVector["nodesVectorHG"][i] = nodesVector["nodesVectorG"][i] - nodesVector["nodesVectorH"][i]
          nodesVector["nodesVectorHC"][i] = nodesVector["nodesVectorC"][i] - nodesVector["nodesVectorH"][i]
          nodesVector["nodesVectorBG"][i] = nodesVector["nodesVectorG"][i] - nodesVector["nodesVectorB"][i]
          nodesVector["nodesVectorGC"][i] = nodesVector["nodesVectorC"][i] - nodesVector["nodesVectorG"][i]
          nodesVector["nodesVectorDH"][i] = nodesVector["nodesVectorH"][i] - nodesVector["nodesVectorD"][i]
    else:
      if firstOrder:
        for i in range(numberOfFaces):
          nodesVector["nodesVectorAC"][i] = nodesVector["nodesVectorC"][i] - nodesVector["nodesVectorA"][i]
      else:
        for i in range(numberOfFaces):
          nodesVector["nodesVectorDE"][i] = nodesVector["nodesVectorE"][i] - nodesVector["nodesVectorD"][i]
          nodesVector["nodesVectorDF"][i] = nodesVector["nodesVectorF"][i] - nodesVector["nodesVectorD"][i]
          nodesVector["nodesVectorEB"][i] = nodesVector["nodesVectorB"][i] - nodesVector["nodesVectorE"][i]
          nodesVector["nodesVectorCF"][i] = nodesVector["nodesVectorF"][i] - nodesVector["nodesVectorC"][i]

    return nodesVector
      

  def __CalculateArea(self, numberOfFaces, nodesVector, Hex = False, firstOrder = False):
    area = numpy.zeros(numberOfFaces)
    if Hex:
      if firstOrder:
        for i in range(numberOfFaces):
          area[i] = 0.5 * (numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAB"][i] , nodesVector["nodesVectorAD"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorDB"][i] , nodesVector["nodesVectorDC"][i])))
          
      else:
        for i in range(numberOfFaces):
          area[i] = 0.5 * (numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAF"][i] , nodesVector["nodesVectorAE"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorFB"][i] , nodesVector["nodesVectorFG"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEF"][i] , nodesVector["nodesVectorEG"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEG"][i] , nodesVector["nodesVectorEH"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEH"][i] , nodesVector["nodesVectorED"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorHG"][i] , nodesVector["nodesVectorHC"][i])))

    else:
      if firstOrder:
        for i in range(numberOfFaces):
          area[i] = 0.5 * numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAB"][i] , nodesVector["nodesVectorAC"][i]))
      
      else:
        for i in range(numberOfFaces):
          area[i] = 0.5 * (numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAE"][i] , nodesVector["nodesVectorAD"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorDE"][i] , nodesVector["nodesVectorDF"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorDF"][i] , nodesVector["nodesVectorDC"][i]))+ \
                           numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEB"][i] , nodesVector["nodesVectorEF"][i])))
  
    return area
  
  
  def __CalculateNormalVector(self, numberOfFaces, nodesVector, Hex = False, firstOrder = False):
    normalVector = numpy.zeros((numberOfFaces,3))
    
    if Hex:
      if firstOrder:
        for i in range(numberOfFaces):
          normalVector[i] = 1.0/2*(numpy.cross(nodesVector["nodesVectorAB"][i], nodesVector["nodesVectorAD"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAB"][i] , nodesVector["nodesVectorAD"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorDB"][i], nodesVector["nodesVectorDC"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorDB"][i] , nodesVector["nodesVectorDC"][i])))
                                   
          normalVector[i] = normalVector[i]/numpy.linalg.norm([normalVector[i,0] , normalVector[i,1], normalVector[i,2]])
      else:
        for i in range(numberOfFaces):
          normalVector[i] = 1.0/6*(numpy.cross(nodesVector["nodesVectorAF"][i], nodesVector["nodesVectorAE"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAF"][i] , nodesVector["nodesVectorAE"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorFB"][i], nodesVector["nodesVectorFG"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorFB"][i] , nodesVector["nodesVectorFG"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorEF"][i], nodesVector["nodesVectorEG"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEF"][i] , nodesVector["nodesVectorEG"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorEG"][i], nodesVector["nodesVectorEH"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEG"][i], nodesVector["nodesVectorEH"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorEH"][i], nodesVector["nodesVectorED"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEH"][i], nodesVector["nodesVectorED"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorHG"][i], nodesVector["nodesVectorHC"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorHG"][i], nodesVector["nodesVectorHC"][i])))
                                   
          normalVector[i] = normalVector[i]/numpy.linalg.norm([normalVector[i,0] , normalVector[i,1], normalVector[i,2]]) 
        
    else:
      if firstOrder:
        for i in range(numberOfFaces):
          normalVector[i] = numpy.cross(nodesVector["nodesVectorAB"][i], nodesVector["nodesVectorAC"][i])/ \
                            numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAB"][i] , nodesVector["nodesVectorAC"][i]))
          
      else:
        for i in range(numberOfFaces):
          normalVector[i] = 1.0/4*(numpy.cross(nodesVector["nodesVectorAE"][i], nodesVector["nodesVectorAD"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorAE"][i] , nodesVector["nodesVectorAD"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorDE"][i], nodesVector["nodesVectorDF"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorDE"][i] , nodesVector["nodesVectorDF"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorDF"][i], nodesVector["nodesVectorDC"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorDF"][i] , nodesVector["nodesVectorDC"][i]))+ \
                                   numpy.cross(nodesVector["nodesVectorEB"][i], nodesVector["nodesVectorEF"][i])/ \
                                   numpy.linalg.norm(numpy.cross(nodesVector["nodesVectorEB"][i], nodesVector["nodesVectorEF"][i])))
                                   
          normalVector[i] = normalVector[i]/numpy.linalg.norm([normalVector[i,0] , normalVector[i,1], normalVector[i,2]])     
    
    return normalVector
    
  def __GetNumber(self, array):
    number = 0
    for i in range(numpy.size(array[:])):
      if int(array[i+1] - array[i]) != 1:
          number = int(array[i])
          break
    return number
  
  
  def __GetCoordinateFromLine(self, splitLine, value):
    if numpy.abs(splitLine[0]) > 0.5:
      return value/splitLine[0] - splitLine[1]/splitLine[0]
    else:
      return value*splitLine[0] + splitLine[1]
    
    
  def __GetIndex(self, coordinateIndex, splitLine):
    index = coordinateIndex
    if numpy.abs(splitLine[0]) < 0.5 and splitLine[0] != 0:
      index[0] == coordinateIndex[1]
      index[1] == coordinateIndex[0]
    return index
  
  
  def __GetIndexFromString(self, string):
    if len(string) > 3:
      nodeList = ["A", "B", "C", "D", "E", "F", "G", "H"]
      for i in range(numpy.size(nodeList)):
        if string[-1] == nodeList[i]:
          return i + 1
    else:
      if string[0][0] == "x":
        return 0
      elif string[0][0] == "y":
        return 1
      else:
        return 2
    
    
  def __GetValuesForPlane(self, value, coordinateIndex, splitLine):
    if splitLine[0] != 0:
      if coordinateIndex[0] == value:
        return -value*splitLine[0]
      elif coordinateIndex[1] == value:
        return 1.0
      else:
        return 1.e-30
    else:
      if coordinateIndex[0] == value:
        return 1.0
      else:
        return 1.e-30
        


  def __CalculateIntersectionPoint(self, arrayForInterpolating, nodesArray, splitLine, coordinateIndex, elementsDict, nodesVector, Hex = False, firstOrder = False):
    #Build the plane (xValue*x + yValue*y + zValue*z = dValue)
    xValue = self.__GetValuesForPlane(1, coordinateIndex, splitLine)
    yValue = self.__GetValuesForPlane(2, coordinateIndex, splitLine)
    zValue = self.__GetValuesForPlane(3, coordinateIndex, splitLine)
    dValue = splitLine[1]

    intersectionPoints = []
    
    if Hex:
      if firstOrder:
        vectorNames = ["nodesVectorBC",  "nodesVectorAB", "nodesVectorAD", "nodesVectorDC"]
      else:
        vectorNames = ["nodesVectorAF",  "nodesVectorAE", "nodesVectorFB", "nodesVectorED", "nodesVectorBG", "nodesVectorGC", "nodesVectorDH", "nodesVectorHC"]
    else:
      if firstOrder:
        vectorNames = ["nodesVectorAB", "nodesVectorAC", "nodesVectorBC"]
      else:
        vectorNames = ["nodesVectorAE", "nodesVectorAD", "nodesVectorFB", "nodesVectorCF", "nodesVectorDC", "nodesVectorEB"]
    
    for key in elementsDict:
      for i in range(numpy.size(vectorNames)):
        coefficient = (dValue - xValue*nodesVector[vectorNames[i][:-1]][key,0] - yValue*nodesVector[vectorNames[i][:-1]][key,1] \
                       - zValue*nodesVector[vectorNames[i][:-1]][key,2])/(xValue*nodesVector[vectorNames[i]][key,0] \
                       + yValue*nodesVector[vectorNames[i]][key,1] + zValue*nodesVector[vectorNames[i]][key,2])
        if coefficient >= 0 and coefficient <= 1:
          intersectionPoint = nodesVector[vectorNames[i][:-1]][key] + coefficient * nodesVector[vectorNames[i]][key]
          firstIndex = self.__GetIndexFromString(vectorNames[i][:-1])
          secondIndex = self.__GetIndexFromString(vectorNames[i])
          nodesList = [int(nodesArray[int(arrayForInterpolating[key, firstIndex] -1), 0]), int(nodesArray[int(arrayForInterpolating[key,secondIndex] -1), 0])]
          nodesList.extend(intersectionPoint)
          intersectionPoints.append(nodesList)

    return intersectionPoints
