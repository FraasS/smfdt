import numpy
import os
from .calculateInterpolation import calculateInterpolation

class writeSolverFile:
  def __init__(self, solverFile, path = "./", elementName = "", boundaryName = [], setName = [], force = False, setNameForce = ["none"], omega = 0, \
               rotationAxis = [0, 0, 0, 0, 0, 0.1], eModul = "210000.0E6", density = "7.8E3", poissonCoefficient = ".3", solver = "SPOOLES"):
    #
    # init variables
    #
    self.solverFile_ = solverFile
    self.path_ = path
    self.elementName_ = elementName
    self.boundaryName_ = boundaryName
    self.setName_ = setName
    self.force_ = force
    self.setNameForce_ = setNameForce
    self.omega_ = omega
    self.rotationAxis_ = rotationAxis
    self.eModul_ = eModul
    self.density_ = density
    self.poissonCoefficient_ = poissonCoefficient
    self.solver_ = solver
     
  #
  # public
  #
  
  def WriteSolverFileIcem(self):
    #Write the solver File
    pathSolverFile = os.path.join(self.path_, self.solverFile_ + ".inp")
    pathNewSolverFile = os.path.join(self.path_, self.solverFile_ + "_final.inp")
    
    l = list(open(pathSolverFile, "r"))
    f = open(pathNewSolverFile, "w")
    for no, line in enumerate(l):
      if "*NSET" in line:
        breakingLine = no
        for nom, line in enumerate(l):
          if nom < breakingLine:
            f.write(line)
        break
    f.close()
    #Find the number of Elements and Nodes
    nodesElementsNumber = numpy.genfromtxt(pathNewSolverFile, comments = '*', delimiter = ',', usecols = 0, skip_header = 3)
    numberOfNodes = int(numpy.max(nodesElementsNumber))
    elementsNumber = numpy.zeros(numpy.size(nodesElementsNumber[:]) - numberOfNodes)
    
    C3D20 = bool(1)
    for i in range(numpy.size(elementsNumber[:])):
      i += numberOfNodes
      if (int(nodesElementsNumber[i]) == int(nodesElementsNumber[i-1]) - 1) or (int(nodesElementsNumber[i]) == int(nodesElementsNumber[i-1]) + 1):
        C3D20 = bool(0) 
    if C3D20:
      for i in range(int(numpy.size(elementsNumber[:])/2)):
        elementsNumber[i] = nodesElementsNumber[i*2+numberOfNodes]
        
    else:
      elementsNumber = nodesElementsNumber[-(numpy.size(nodesElementsNumber[:])-numberOfNodes):]
    numberOfElements = int(numpy.max(elementsNumber))
    for i in range(numpy.size(self.boundaryName_)):
      breakingLine = 0
      f = open(pathNewSolverFile, "a+")
      for no, line in enumerate(l):
        if self.boundaryName_[i] in line:
          f.write(line)
          breakingLine = no
          break
      for nom, line in enumerate(l):
        if nom > breakingLine:
          if "*" in line:
            break
          else:
            f.write(line)
      f.close()
    self.__WriteSolverFile(pathNewSolverFile, numberOfNodes, numberOfElements, Hex = True)


  def WriteSolverFileGmsh(self):
    # Write the solver File
    pathInterpolateFile = os.path.join(self.path_, self.solverFile_ + "_interpolate.inp")
    pathNewSolverFile = os.path.join(self.path_, self.solverFile_ + "_final.inp")

    breakingLineNodes = 0
    startLineElements = 0
    endLineElements = 0
    l = list(open(pathInterpolateFile, "r"))
    f = open(pathNewSolverFile, "w")
    for no, line in enumerate(l):
      if "E L" in line:
        breakingLineNodes = no
      elif "C3" in line:
        startLineElements = no
      elif "*ELSET" in line:
        endLineElements = no
        break
    for nom, line in enumerate(l):
      if nom < breakingLineNodes:
        f.write(line)
      elif nom < endLineElements and nom >= startLineElements:
        f.write(line)
      elif nom > endLineElements:
        break
    f.close()

    # Find the number of Elements and Nodes
    nodesElementsNumber = numpy.genfromtxt(pathNewSolverFile, comments='*', delimiter=',', usecols=0, skip_header=3)
    numberOfNodes = self.__GetNumber(nodesElementsNumber)

    elementsNumber = nodesElementsNumber[-(numpy.size(nodesElementsNumber[:]) - numberOfNodes):]
    elementsMin = int(numpy.min(elementsNumber))
    elementsMax = int(numpy.max(elementsNumber))
    numberOfElements = elementsMax - elementsMin + 1

    # correct the elements numbering
    elementArray = numpy.genfromtxt(pathNewSolverFile, comments='*', delimiter=',', skip_header=3 + numberOfNodes)
    for i in range(numpy.size(elementArray[:, 0])):
      elementArray[i, 0] = i + 1

    f = open(pathNewSolverFile, "w")
    for nom, line in enumerate(l):
      if nom <= breakingLineNodes:
        f.write(line)
      elif nom == startLineElements:
        f.write(line)
      elif nom > startLineElements:
        break
    numpy.savetxt(f, elementArray, fmt="% u", comments='', delimiter=',')
    f.close()
    for i in range(numpy.size(self.boundaryName_)):
      f = open(pathNewSolverFile, "a+")
      for no, line in enumerate(l):
        if os.path.join("NSET=" + self.boundaryName_[i]) in line:
          breakingLine = no
          for nom, line in enumerate(l):
            if nom == breakingLine:
              f.write(line)
            elif nom > breakingLine:
              if "*" in line:
                break
              else:
                f.write(line)
      f.close()
    self.__WriteSolverFile(pathNewSolverFile, numberOfNodes, numberOfElements, Hex=False)


  def WriteSolverInterpolateFile(self):
    #Write the solver File for Interpolation
    pathSolverFile = os.path.join(self.path_, self.solverFile_ + ".inp")
    pathSolverInterpolateFile = os.path.join(self.path_, self.solverFile_ + "_interpolate.inp")
    startLineNSet, nodesPerLine, nodesLastLine = [], [], []
    startLineElements = 0

    # Save the node sets in a dict
    nodeSetDict = dict()
    nodeSetDictLastLine = dict()

    l = list(open(pathSolverFile, "r"))
    for no, line in enumerate(l):
      if "NODE" in line:
        startLineNodes = no + 1
      if "E L" in line:
        breakingLineNodes = no
      if "C3" in line and startLineElements == 0:
        startLineElements = no
      if "*ELSET" in line:
        endLineElements = no
        break
    for no, line in enumerate(l):
      if "*NSET" in line:
        startLineNSet.append(no + 1)
        nodesPerLine.append(len((l[no + 1]).split(",")))
        nodeSetDict[line] = ""
        nodeSetDictLastLine[line] = ""
    for no, line in enumerate(l):
      if no >= startLineNSet[0]:
        if "*" in line:
          nodesLastLine.append(len((l[no-1]).split(",")))
        elif no == len(l) -1:
          nodesLastLine.append(len((l[no]).split(",")))

    f = open(pathSolverInterpolateFile, "w")
    for no, line in enumerate(l):
      if no <= breakingLineNodes:
        f.write(line)
      elif no >= startLineElements and no < endLineElements:
        f.write(line)
    f.close()

    # Find the number of Elements and Nodes
    nodesElementsNumber = numpy.genfromtxt(pathSolverInterpolateFile, comments='*', delimiter=',', usecols=0, skip_header=startLineNodes - 1)

    numberOfNodes = breakingLineNodes - startLineNodes
    numberLastNode = int(nodesElementsNumber[numberOfNodes - 1])

    elementsNumber = nodesElementsNumber[-(numpy.size(nodesElementsNumber[:]) - numberOfNodes):]
    elementsMin = int(numpy.min(elementsNumber))
    elementsMax = int(numpy.max(elementsNumber))
    numberOfElements = elementsMax - elementsMin + 1

    # Read the element and node arrays
    elementArray = numpy.genfromtxt(pathSolverInterpolateFile, comments='*', delimiter=',', skip_header=3 + numberOfNodes)
    nodeArray = numpy.genfromtxt(pathSolverInterpolateFile, comments='*', delimiter=',', skip_header=3, skip_footer=numberOfElements)

    # Read the node sets arrays
    skipFooterList = [0]*numpy.size(startLineNSet)
    for i in range(numpy.size(startLineNSet) -1 ):
      skipFooterList[i] = len(l) - startLineNSet[i+1] + 1

    counter = 0
    for key in nodeSetDict:
      nodeSetDict[key] = \
        numpy.genfromtxt(pathSolverFile, delimiter=',', usecols=range(0,nodesPerLine[counter]-1), \
                         skip_header=startLineNSet[counter], skip_footer=skipFooterList[counter] + 1)
      
      nodeSetDictLastLine[key] = \
        numpy.genfromtxt(pathSolverFile, delimiter=',', usecols=range(0,nodesLastLine[counter]-1), \
                         skip_header=len(l) - skipFooterList[counter] - 1, skip_footer=skipFooterList[counter])

      counter += 1

    # correct the node numbering if it's wrong
    if numberLastNode != numberOfNodes:
      correctionDict = self.__GetCorrectionDict(nodeArray[:, 0])
      nodeArray = self.__CorrectNodeNumbers(nodeArray, correctionDict, 0, 1)
      elementArray = self.__CorrectNodeNumbers(elementArray, correctionDict, 1, elementArray.shape[1])
      counter = 0
      for key in nodeSetDict:
        nodeSetDict[key] = self.__CorrectNodeNumbers(nodeSetDict[key], correctionDict, 0, nodesPerLine[counter] -1)
        if nodeSetDictLastLine[key] != "":
          nodeSetDictLastLine[key] = self.__CorrectNodeNumbers(nodeSetDictLastLine[key], correctionDict)
        counter += 1

    # Write the interpolating File
    f = open(pathSolverInterpolateFile, "w")
    for nom, line in enumerate(l):
      if nom < startLineNodes:
        f.write(line)
      elif nom == startLineElements:
        numpy.savetxt(f, nodeArray, fmt="%u, % s, % s, % s", comments='', delimiter=',')
        f.write(l[breakingLineNodes])
        f.write(line)
        numpy.savetxt(f, elementArray, fmt="% u", comments='', delimiter=',')
      elif nom >= endLineElements:
        if "*NSET" in line:
          break
        else:
          f.write(line)
    for key in nodeSetDict:
      f.write(key)
      numpy.savetxt(f, nodeSetDict[key], fmt="%u,", delimiter=' ')
      f.write(self.__GetStringFromArray(nodeSetDictLastLine[key]))
      f.write("\n")
    f.close()

  #
  #private functions  
  #

  def __GetNumber(self, array):
    number = 0
    for i in range(numpy.size(array[:])):
      if int(array[i+1] - array[i]) != 1:
          number = int(array[i])
          break
    return number


  def __GetCorrectionDict(self, array):
    number = 0
    correctionDict = dict()
    for i in range(numpy.size(array[:])):
      if number != 0:
        correctionDict[int(array[i])] = int(array[i]) - number
      try:
        if int(array[i+1] - array[i]) != 1:
          number += int(array[i+1] - array[i]) - 1
      except IndexError:
        pass
    return correctionDict


  def __CorrectNodeNumbers(self, array, correctionDict, lowerLimit = 0, upperLimit = 1):
    try:
      for j in range(lowerLimit, upperLimit):
        for k in range(numpy.size(array[:,j])):
          if array[k,j] in correctionDict:
            array[k,j] = int(correctionDict[array[k,j]])
    except IndexError:
      try:
        for j in range(numpy.size(array)):
          if array[j] in correctionDict:
            array[j] = int(correctionDict[array[j]])
      except IndexError:
        if int(array) in correctionDict:
          array = int(correctionDict[int(array)])
    return array


  def __GetStringFromArray(self, array):
    string = ""
    try:
      for i in range(numpy.size(array)):
        string = os.path.join(string + str(int(array[i])) + ", ")
    except TypeError:
      string = os.path.join(string + str(int(array)) + ", ")
    except IndexError:
      string = os.path.join(string + str(int(array)) + ", ")
    return string


  def __GetPressureValues(self):
    for k in range(1,3):
      pathDatFile = os.path.join(self.path_, self.setName_[0] + "_ds" + str(k) + "e1.dlo")
      if os.path.exists(pathDatFile):
        # Load the interpolated node Values
        pressureValues = numpy.genfromtxt(pathDatFile, delimiter=',', comments='*', dtype=str)
      else:
        #file does not exist, do nothing
        pass

    pathMshFile = os.path.join(self.path_, self.elementName_ + ".msh")
    # Find the lowest element number in the setData to correct the element number for the solver File
    elementNumberArray = numpy.genfromtxt(pathMshFile, usecols=0, comments='*', delimiter=',')
    try:
      elementNumberArray = numpy.genfromtxt(pathMshFile, comments='*', delimiter=',', usecols=0)
      elementNumber = int(numpy.min(elementNumberArray))
    except ValueError:
      os.remove(os.path.join(self.path_, self.solverFile_ + "_final.inp"))
      raise RuntimeError("Use C3D8 elements for interpolationg instead of C3D20") from ValueError

    # Correct the element number
    for i in range(numpy.size(pressureValues[:, 0])):
      pressureValues[i, 0] = int(pressureValues[i, 0]) - elementNumber + 1
    return pressureValues


  def __WriteSolverFile(self, pathNewSolverFile, numberOfNodes, numberOfElements, Hex):
    f = open(pathNewSolverFile, "a+")
    f.write("*NSET,NSET=NALL,GENERATE\n1,")
    f.write(str(numberOfNodes))
    f.write("\n*ELSET,ELSET=EALL,GENERATE\n1,")
    f.write(str(numberOfElements))
    for i in range(numpy.size(self.boundaryName_)):
      f.write("\n*BOUNDARY\n")
      f.write(self.boundaryName_[i])
      f.write(", 1\n*BOUNDARY\n")
      f.write(self.boundaryName_[i])
      f.write(", 2\n*BOUNDARY\n")
      f.write(self.boundaryName_[i])
      f.write(", 3")
    f.write("\n*MATERIAL, NAME=EL\n*ELASTIC\n ")
    f.write(self.eModul_)
    f.write(",        ")
    f.write(self.poissonCoefficient_)
    f.write("\n*DENSITY\n")
    f.write(self.density_)
    f.write("\n*SOLID SECTION,ELSET=EALL,MATERIAL=EL\n*****\n")
    if self.force_:
      # Write interpolated values, pressure and shear as node force
      interpolatedValues = calculateInterpolation(self.path_, self.solverFile_, self.elementName_, self.setName_).CalculateNodeValues(Hex)
      f.write("*STEP\n*STATIC,SOLVER=")
      f.write(self.solver_)
      f.write("\n*CLOAD\n")
      numpy.savetxt(f, interpolatedValues, fmt="%u, %u, %f", comments='', delimiter=',')
    else:
      # Write interpolated values, pressure as surface load and shear as node force
      pressureValues = self.__GetPressureValues()
      f.write("*STEP\n*STATIC,SOLVER=")
      f.write(self.solver_)
      f.write("\n*DLOAD\n")
      numpy.savetxt(f, pressureValues, fmt="%s, %s, %s", comments='', delimiter=',')
      if self.setNameForce_[0] != "none":
        interpolatedValues = calculateInterpolation(self.path_, self.solverFile_, self.elementName_, self.setNameForce_).CalculateNodeValues(Hex)
        f.write("*CLOAD\n")
        numpy.savetxt(f, interpolatedValues, fmt="%u, %u, %f", comments='', delimiter=',')
    if self.omega_ != 0:
      f.write("*DLOAD\nEALL,CENTRIF,")
      f.write(str(numpy.power(self.omega_, 2)))
      for i in range(6):
        f.write(",")
        f.write(str(self.rotationAxis_[i]))
    f.write("\n*NODE PRINT,NSET=NALL\nU\n*EL PRINT,ELSET=EALL\nS\n*Node File\nU\n*El File\nS\n*END STEP")
    f.close()