import numpy
import os
from .calculateInterpolation import calculateInterpolation

class calculixPost:
  def __init__(self, pathAndDataArray):
    #
    # init variables
    #
    self.path_ = pathAndDataArray[0]
    self.solverFile_ = pathAndDataArray[1]
    self.dataArray_ = pathAndDataArray[2]
    
    
  #
  # public
  #
    
  def WriteNodeValuesToCsv(self, dataFileName, coordinate = [], splitLinePoints = ["none"], lineDirection = "x", function = "y>"):    
    function = function.replace(" ","")
    #Write the csv list
    f = open(os.path.join(self.path_, dataFileName + ".csv"), "w")
    f.write("# 1 nodeNumber\n# 2 x-coordinate\n# 3 y-coordinate\n# 4 z-coordinate\n# 5 dx\n# 6 dy\n# 7 dz\n# 8 Mises\n")
    f.close()
    for i in range(numpy.size(self.dataArray_[:,0])):       
      #write one point to csv  
      if numpy.size(coordinate) == 3:
        if numpy.abs(self.dataArray_[i,self.__GetIndexFromString(coordinate[0])] + self.__GetOperationFromString(coordinate[0])) < 0.0001 and \
           numpy.abs(self.dataArray_[i,self.__GetIndexFromString(coordinate[1])] + self.__GetOperationFromString(coordinate[1])) < 0.0001 and \
           numpy.abs(self.dataArray_[i,self.__GetIndexFromString(coordinate[2])] + self.__GetOperationFromString(coordinate[2])) < 0.0001:
          self.__AppendCsv(dataFileName, i)
            
      elif splitLinePoints[0] == "none":
        if numpy.abs(self.dataArray_[i,self.__GetIndexFromString(coordinate[0])] + self.__GetOperationFromString(coordinate[0])) < 0.0001:
          self.__AppendCsv(dataFileName, i)
          
      else:
        #Calculate splitLine
        functionValueIndex = self.__GetIndexFromString(function)
        functionVariable = self.__GetIndexFromString(lineDirection)
        #dummy coordinateIndex, only necessary for WriteLineValuesToCsv
        splitLineCoefficients = self.__GetSplitLineAndIndex(splitLinePoints, 1)[0]
        functionValue = splitLineCoefficients[0] * self.dataArray_[i,functionVariable] + splitLineCoefficients[1]
        if self.__GetOperationFromString(function) == ">":
          if numpy.abs(self.dataArray_[i,self.__GetIndexFromString(coordinate[0])] + self.__GetOperationFromString(coordinate[0])) < 0.0001 \
             and self.dataArray_[i][functionValueIndex] > functionValue:
            self.__AppendCsv(dataFileName, i)
        else:
          if numpy.abs(self.dataArray_[i,self.__GetIndexFromString(coordinate[0])] + self.__GetOperationFromString(coordinate[0])) < 0.0001 \
             and self.dataArray_[i][functionValueIndex] < functionValue:
            self.__AppendCsv(dataFileName, i)
        
        
  def WriteLineValuesToCsv(self, dataFileName, elementName, setName = [], splitLinePoints = [], coordinate = [], normalVector = ["none"], \
                           lineDirection = ["none"], Hex = False):
    coordinateIndex = [0]*numpy.size(coordinate)
    for i in range(numpy.size(coordinateIndex)):
      coordinateIndex[i] = self.__GetIndexFromString(coordinate[i])
    #Get the coordinateIndex and splitLine parameters
    coordinateIndex = self.__GetSplitLineAndIndex(splitLinePoints, coordinateIndex)[1]
    splitLine = self.__GetSplitLineAndIndex(splitLinePoints, coordinateIndex)[0]

    #Calculate the line values
    calculatedLineValueArray = calculateInterpolation(self.path_, self.solverFile_, elementName, setName). \
      CalculateLineValues(coordinateIndex, splitLine, normalVector, lineDirection, self.dataArray_, Hex)

    #Write the csv
    f = open(os.path.join(self.path_, dataFileName + ".csv"), "w")
    f.write("# 1 nodeNumber1\n# 2 nodeNumber2\n# 3 x-coordinate\n# 4 y-coordinate\n# 5 z-coordinate\n# 6 dx\n# 7 dy\n# 8 dz\n# 9 Mises\n")
    for i in range(numpy.size(calculatedLineValueArray[:,0])):
      f.write("\n")
      f.write(os.path.join(
        str(int(calculatedLineValueArray[i, 0])) + ", " + str(calculatedLineValueArray[i, 1]) + ", " + str(calculatedLineValueArray[i, 2]) + ", " + \
        str(calculatedLineValueArray[i, 3]) + ", " + str(calculatedLineValueArray[i, 4]) + ", " + str(calculatedLineValueArray[i, 5]) + \
        "," + str(calculatedLineValueArray[i, 6]) + ", " + str(calculatedLineValueArray[i, 7]) + ", " + str(calculatedLineValueArray[i, 8]) ))
    f.close()
    
  
  # Get the area averaged values for dx, dy, dz and mises
  def GetAverageValues(self, elementName, setName = [], Hex = False):
    return calculateInterpolation(self.path_, self.solverFile_, elementName, setName).CalculateAverageValues(self.dataArray_, Hex)
    
    
  def GetMaximum(self):
    maxList = ["dx", "dy", "dz", "Mises"]
    for i in range(4):
      maxValueIndex = numpy.argmax(self.dataArray_[:,i+4])
      maxList[i] = maxList[i], self.dataArray_[maxValueIndex,1], self.dataArray_[maxValueIndex,2], \
                   self.dataArray_[maxValueIndex,3], self.dataArray_[maxValueIndex,i+4]
    return maxList


  def GetMinimum(self):
    minList = ["dx", "dy", "dz", "Mises"]
    for i in range(4):
      minValueIndex = numpy.argmin(self.dataArray_[:,i+4])
      minList[i] = minList[i], self.dataArray_[minValueIndex,1], self.dataArray_[minValueIndex,2], \
                   self.dataArray_[minValueIndex,3], self.dataArray_[minValueIndex,i+4]
    return minList
  
  #
  # private functions
  #
  
  def __GetIndexFromString(self, string):
    string = string.replace(" ","")
    if string[0] == "x":
      return 1
    elif string[0] == "y":
      return 2
    else:
      return 3 
    
    
  def __GetOperationFromString(self, string):
    string = string.replace(" ","")
    if string[1] == "=":  
      return -float(string[2:])
    elif string[1] == ">" or string[1] == "<":
      return string[1]    
    else:
      return float(string[2:])
  
  
  def __GetSplitLineAndIndex(self, splitLinePoints, coordinateIndex):
    index = coordinateIndex
    if splitLinePoints[0] != splitLinePoints[2]:
      slope = (splitLinePoints[3] - splitLinePoints[1])/(splitLinePoints[2] - splitLinePoints[0])
      coordinateAxisIntersection = splitLinePoints[1] - slope * splitLinePoints[0]
      if slope == 0:
        index[0] = coordinateIndex[1]
        index[1] = coordinateIndex[0]
    else:
      slope = 0
      coordinateAxisIntersection = splitLinePoints[0]
    splitLine = [slope, coordinateAxisIntersection]
    return splitLine, index


  def __AppendCsv(self, dataFileName, i): 
    f = open(os.path.join(self.path_, dataFileName + ".csv"), "a+")
    f.write("\n")
    f.write(os.path.join(str(int(self.dataArray_[i,0])) + ", " + str(self.dataArray_[i,1]) + ", " + str(self.dataArray_[i,2]) + ", " + \
            str(self.dataArray_[i,3]) + ", " + str(self.dataArray_[i,4]) + ", " + str(self.dataArray_[i,5]) + \
            ", " + str(self.dataArray_[i,6]) + ", " + str(self.dataArray_[i,7])))
    f.close()
  
    
  
