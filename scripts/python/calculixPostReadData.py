import numpy
import os

class calculixPostReadData:
  def __init__(self, path, fileName, surfaceName = [], scaleStress = 1, scaleDisplacement = 1):
    #
    # init variables
    #
    self.path_ = path
    self.fileName_ = fileName
    self.surfaceName_ = surfaceName
    self.scaleStress_ = scaleStress
    self.scaleDisplacement_ = scaleDisplacement
    

  #
  # public
  #
  
  def Read(self):
    solverInterpolateFile = os.path.join(self.path_, self.fileName_ + "_interpolate.inp")
    dataFile = os.path.join(self.path_, self.fileName_ + "_final.frd")
    dataFileTemp = os.path.join(self.path_, self.fileName_ + "_final_data.frd")
    #Load the nodes
    try:
      #gmsh mesh
      l = list(open(solverInterpolateFile, "r")) 
    except FileNotFoundError:
      #icem mesh
      solverInterpolateFile = os.path.join(self.path_, self.fileName_ + ".inp")
      l = list(open(solverInterpolateFile, "r"))
    for no, line in enumerate(l):
      if "NODE" in line:
        numberHeaderLines = no
        break
      
    nodesNumber = numpy.genfromtxt(solverInterpolateFile, comments = '*', delimiter = ',', usecols = 0, skip_header = numberHeaderLines)
    numberOfNodes = self.__GetNumber(nodesNumber)
    nodesArray = numpy.genfromtxt(solverInterpolateFile, comments = '*', delimiter = ',', skip_header = numberHeaderLines, max_rows = numberOfNodes)

    #Get the displacements and stresses
    l = list(open(dataFile, "r"))
    f = open(dataFileTemp, "w")
    startLine = numpy.size(l)
    for no, line in enumerate(l):
      if "DISP" in line:
        startLine = no
      if no > startLine:
        f.write(line.replace("-", " -"))
    f.close()
    l = list(open(dataFileTemp, "r"))
    f = open(dataFileTemp, "w")
    for no, line in enumerate(l):
      if "ERROR" in line:
        break
      else:
        f.write(line.replace("E -", "E-"))
    f.close()
    
    displacementArray = numpy.genfromtxt(dataFileTemp, delimiter = '', skip_header = 4, max_rows = numberOfNodes)
    try:
      #If ccx version is 2.12
      stressArray = numpy.genfromtxt(dataFileTemp, delimiter = '', skip_header = 14 + numberOfNodes, skip_footer = 2)
    except ValueError:
      #If ccx version is 2.16
      stressArray = numpy.genfromtxt(dataFileTemp, delimiter='', skip_header=14 + numberOfNodes, skip_footer=3)
    
    #get the nodes at the surface
    nodesSurfaceList = []
    for i in range(numpy.size(self.surfaceName_)):
      l = list(open(solverInterpolateFile, "r"))
      f = open(dataFileTemp, "w")
      startLine = 0
      for no, line in enumerate(l):
        if self.surfaceName_[i] in line and "NSET" in line:
          startLine = no
          break
      for nom, line in enumerate(l):
        if nom > startLine:  
          f.write(line.replace(",", ""))
          if "*" in line:
            startLine = numpy.size(l)
      f.close()
      nodesSurfaceArrayLastLine = numpy.zeros(0)
      try:
        nodesSurfaceArray = numpy.genfromtxt(dataFileTemp, comments = '*', delimiter = '')
      except ValueError:
        #Last line with different number of row
        nodesSurfaceArray = numpy.genfromtxt(dataFileTemp, comments='*', delimiter='', skip_footer=1)
        nodesSurfaceArrayLastLine = numpy.genfromtxt(dataFileTemp, comments='*', delimiter='', skip_header=numpy.size(nodesSurfaceArray[:,0]) )

      for k in range(numpy.size(nodesSurfaceArray[:,0])):
        for j in range(numpy.size(nodesSurfaceArray[0,:])):
          nodesSurfaceList.append(nodesSurfaceArray[k,j])

      try:
        for k in range(numpy.size(nodesSurfaceArrayLastLine[:])):
          nodesSurfaceList.append(nodesSurfaceArrayLastLine[k])
      except IndexError:
        #Only one row in last line
        nodesSurfaceList.append(nodesSurfaceArrayLastLine)

    nodesSurfaceList.sort()
    
    os.remove(dataFileTemp)
    
    #Calculate the mises stress
    misesStress = numpy.zeros(numberOfNodes)
    for i in range(numberOfNodes):
      misesStress[i] = numpy.sqrt(0.5*(numpy.power((stressArray[i,2]-stressArray[i,3]),2)+numpy.power((stressArray[i,3]-stressArray[i,4]),2)+ \
                       numpy.power((stressArray[i,4]-stressArray[i,2]),2)+6.0*(numpy.power(stressArray[i,5],2)+numpy.power(stressArray[i,6],2)+ \
                       numpy.power(stressArray[i,7],2) )))
    
    #Buld the data Dict
    dataDict = dict()
    counter = 0
    for i in range(int(numpy.min(nodesSurfaceList)), int(numpy.max(nodesSurfaceList)) + 1):
      if int(nodesSurfaceList[0]) == i:  
        dataList = [0] * 8
        dataList[0] = nodesArray[i-1,0]
        dataList[1] = nodesArray[i-1,1]
        dataList[2] = nodesArray[i-1,2]
        dataList[3] = nodesArray[i-1,3]
        dataList[4] = self.scaleDisplacement_ * displacementArray[i-1,2]
        dataList[5] = self.scaleDisplacement_ * displacementArray[i-1,3]
        dataList[6] = self.scaleDisplacement_ * displacementArray[i-1,4]
        dataList[7] = self.scaleStress_ * misesStress[i-1]
        dataDict[counter] = dataList
        counter += 1
        for j in range(numpy.size(self.surfaceName_)):
          try:
            if int(nodesSurfaceList[0]) == i:
              nodesSurfaceList.pop(0)
          except IndexError:
            pass
            
    #Build an Array from the Dict 
    dataArray = numpy.zeros((len(dataDict), 8))
    for key in dataDict:
      dataArray[key] = dataDict[key]

    return self.path_, self.fileName_, dataArray

  #
  # private functions
  #
  
  def __GetNumber(self, array):
    number = 0
    for i in range(numpy.size(array[:])):
      if int(array[i+1] - array[i]) != 1:
        number = int(array[i])
        break
    return number
