import numpy
import os
import re

class writeShearStressAndPressure:
  def __init__(self, path, timestep, patchName = [], referencePressure = 0):
    #
    # init variables
    #
    self.path_ = path
    self.timestep_ = timestep
    self.patchName_ = patchName
    self.referencePressure_ = referencePressure   
  
  #
  # public
  #
  
  def WriteShearStress(self):
    FileF = os.path.join(self.path_, self.timestep_, "tauWall")
    self.__WriteFoamHeader(FileF)

    for k in range(numpy.size(self.patchName_)):

      wallShearStress = self.__ReadData("wallShearStress", k)
    
      wallShearStressArray = numpy.zeros((numpy.size(wallShearStress[:,0]),3))
      for i in range(numpy.size(wallShearStress[:,0])):
        wallShearStressArray[i,0] = -1000 * wallShearStress[i,3]
        wallShearStressArray[i,1] = -1000 * wallShearStress[i,4]
        wallShearStressArray[i,2] = -1000 * wallShearStress[i,5] 
        
      self.__WriteValues(FileF, wallShearStressArray, k)
    
    f = open(FileF, "a+")
    f.write("}")
    f.close()  


  def WritePressure(self):
    FileP = os.path.join(self.path_, self.timestep_, "P")
    self.__WriteFoamHeader(FileP)

    for k in range(numpy.size(self.patchName_)):

      pressure = self.__ReadData("p", k)
        
      pressureArray = numpy.zeros(numpy.size(pressure[:,0]))
      
      for i in range(numpy.size(pressure[:,0])):
        pressureArray[i] = 1000*(pressure[i,3] + self.referencePressure_) 
        
      self.__WriteValues(FileP, pressureArray, k)
    
    f = open(FileP, "a+")
    f.write("}")
    f.close()
  
      
  def WritePressureAnsysMechanical(self):
    FileP = os.path.join(self.path_, "ansysMechanicalPressure" + ".txt")
    try:
      os.remove(FileP)
    except FileNotFoundError:
      pass
    
    for k in range(numpy.size(self.patchName_)):  
      
      pressure = self.__ReadData("p", k)
      
      for i in range(numpy.size(pressure[:,0])):
        pressure[i,3] = 1000*(pressure[i,3] + self.referencePressure_) 
        
      f = open(FileP, "a+")
      numpy.savetxt(f, pressure, fmt = "%f",comments='')
      f.close()
          
  #
  # private functions
  #
    
  def __WriteFoamHeader(self, filePath):   
    f = open(filePath, "w")
    f.write("/*--------------------------------*- C++ -*----------------------------------*\\\n")
    f.write("| =========                 |                                                 |\n")
    f.write("| \\\      /  F ield         | foam-extend: Open Source CFD                    |\n")
    f.write("|  \\\    /   O peration     | Version:     3.1                                |\n")
    f.write("|   \\\  /    A nd           | Web:         http://www.extend-project.de       |\n")
    f.write("|    \\\/     M anipulation  |                                                 |\n")
    f.write("\*---------------------------------------------------------------------------*/\n")
    f.write("FoamFile\n{\n    version     2.0;\n    format      ascii;\n")
    try:
      re.findall("P", filePath)[0]
      f.write("    class       volScalarField;\n    location    \"")
      f.write(str(self.timestep_))
      f.write("\";\n    object      P;\n}\n")
      f.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n\n")
      f.write("dimensions      [1 -1 -2 0 0 0];\n\n")
    except IndexError:
      f.write("    class       volVectorField;\n    location    \"")
      f.write(str(self.timestep_))
      f.write("\";\n    object      tauWall;\n}\n")
      f.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n\n")
      f.write("dimensions      [1 1 -2 0 0 0];\n\n")
    f.close()

  def __ReadData(self, variableName, k):
    dataName = os.path.join(self.path_, self.patchName_[k] + "_" + variableName + "_" + self.timestep_ + ".csv")
    return numpy.genfromtxt(dataName, delimiter = ',', comments = '#')
                            
  def __WriteValues(self, filePath, valueArray, k):
    f = open(filePath, "a+")
    if k == 0:
      f.write("boundaryField\n{\n")
    f.write(self.patchName_[k])
    try:
      re.findall("P", filePath)[0]
      f.write("\n {\n        type        fixedValue;\n        value       nonuniform List<scalar>\n  \n")
      f.write(str(numpy.size(valueArray[:])))
      numpy.savetxt(f, valueArray, fmt = "(%f) ",comments='', header = '\n(', footer = ')\n;\n    }')
    except IndexError:
      f.write("\n {\n        type        fixedValue;\n        value       nonuniform List<vector>\n  \n")
      f.write(str(numpy.size(valueArray[:,0])))
      numpy.savetxt(f, valueArray, fmt = "(%f %f %f) ",comments='', header = '\n(', footer = ')\n;\n    }')
    f.close()